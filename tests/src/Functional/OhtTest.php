<?php

namespace Drupal\Tests\tmgmt_oht\Functional;

use Drupal\Tests\tmgmt\Functional\TMGMTTestBase;
use Drupal\tmgmt\Entity\Translator;
use Drupal\Core\Url;
use Drupal\tmgmt\Entity\RemoteMapping;

/**
 * Tests the Oht translator plugin integration.
 *
 * @group tmgmt_oht
 */
class OhtTest extends TMGMTTestBase {

  /**
   * @var \Drupal\tmgmt\Entity\Translator $translator
   */
  private $translator;

  protected static $modules = array(
    'tmgmt_oht',
    'tmgmt_oht_test',
  );

  public function setUp(): void {
    parent::setUp();
    $this->addLanguage('de');
    $this->translator = Translator::load('oht');
    \Drupal::configFactory()->getEditable('tmgmt_oht.settings')->set('use_mock_service', TRUE)->save();
  }

  /**
   * Tests basic API methods of the plugin.
   */
  public function testAPI() {
    $job = $this->createJob();
    $job->translator = $this->translator->id();
    \Drupal::state()->set('tmgmt.test_source_data',  array(
      'wrapper' => array(
        '#text' => 'Hello world',
        '#label' => 'Wrapper label',
      ),
    ));
    $item = $job->addItem('test_source', 'test', '1');
    $item->save();

    // The translator should not be available at this point because we didn't
    // define an API key yet.
    $this->assertFalse($job->canRequestTranslation()->getSuccess());

    $this->translator = $job->getTranslator();

    // Test that translation requests are rejected due to a wrong API key.
    $this->translator->setSetting('api_public_key', 'wrong key');
    $this->translator->setSetting('api_secret_key', 'wrong key');
    $this->translator->save();

    $this->translator->clearLanguageCache();

    $this->assertFalse($job->canRequestTranslation()->getSuccess());

    // Save a correct api key.
    $this->translator->setSetting('api_public_key', 'correct key');
    $this->translator->setSetting('api_secret_key', 'correct key');
    $this->translator->save();
    $this->assertTrue($job->canRequestTranslation()->getSuccess());

    $this->translator->clearLanguageCache();

    // Create a new job.
    $job = $this->createJob();
    $job->translator = $this->translator->id();
    $job->addItem('test_source', 'test', '1');

    // Make sure the translator returns the correct supported target languages.
    $languages = $job->getTranslator()->getSupportedTargetLanguages('en');
    $this->assertTrue(isset($languages['de']));
    $this->assertTrue(isset($languages['es']));
    $this->assertFalse(isset($languages['it']));
    $this->assertFalse(isset($languages['en']));

    // Request translation and verify that the state of the job and job item is
    // correctly updated.
    $job->requestTranslation();
    $this->assertTrue($job->isActive());
    foreach ($job->getItems() as $item) {
      $this->assertTrue($item->isActive());
    }

    // Verify that the xliff file submitted is correct.
    $xliff_content = \Drupal::state()->get('tmgmt_oht_test_xliff_file_content');
    $this->assertStringContainsString('source-language="en-us" target-language="de-de"', $xliff_content);
    $this->assertStringContainsString('<source xml:lang="en-us"><![CDATA[Hello world]]></source>', $xliff_content);

    // Retrieve the resource uuid and project id.
    $resource_uuid = \Drupal::state()->get('tmgmt_oht_test_source_resource_uuid');
    $project_id = \Drupal::state()->get('tmgmt_oht_test_project_id');

    // Create a BLEND response of a completed job.
    $post = [
      'event' => 'project.resources.new',
      'project_id' => $project_id,
      'project_status_code' => 'completed',
      'resource_uuid' => $resource_uuid,
      'resource_type' => 'translation',
      'custom0' => \Drupal::state()->get('tmgmt_oht_test_tjiid'),
      'custom1' => \Drupal::state()->get('tmgmt_oht_test_tjiid_hash'),
    ];

    $action = Url::fromRoute('tmgmt_oht.callback')->setOptions(array('absolute' => TRUE))->toString();
    $out = \Drupal::httpClient()->request('POST', $action, array('form_params' => $post));

    // Response should be empty if everything went ok.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertEmpty((string) $out->getBody());

    // Clear job item caches.
    \Drupal::entityTypeManager()->getStorage('tmgmt_job_item')->resetCache();

    // Now it should be needs review.
    foreach ($job->getItems() as $item) {
      $this->assertTrue($item->isNeedsReview());
    }

    $items = $job->getItems();
    $item = end($items);
    $data = $item->getData();
    $this->assertEquals($data['wrapper']['#translation']['#text'], 'Hallo Wort');
  }

  /**
   * Test that the checkout form contains the correct expertise levels and
   * displays the correct quote and balance.
   */
  public function testOhtCheckoutForm() {
    $this->loginAsAdmin();
    $this->translator->setSetting('api_public_key', 'correct key');
    $this->translator->setSetting('api_secret_key', 'correct key');
    $this->translator->save();

    // Create a new job.
    $job = $this->createJob();
    $job->translator = $this->translator->id();
    $job->save();
    $item = $job->addItem('test_source', 'test', '1');
    $item->save();

    $this->drupalGet('admin/tmgmt/jobs/' . $job->id());
    $mocked_quote = \Drupal::state()->get('tmgmt_oht_test_quote_total');
    $mocked_expertise = \Drupal::state()->get('tmgmt_oht_test_quote_total');

    // Verify that the expertise dropdown menu contains expertise returned
    // by the mock server and that the default selected one is "-".
    $expertise = $this->xpath('//select[@id="edit-settings-expertise"]/option');
    $this->assertEmpty($expertise[0]->getValue());
    $this->assertTrue($expertise[0]->isSelected());
    $this->assertSession()->optionExists('edit-settings-expertise', 'automotive-aerospace');
    $this->assertSession()->optionExists('edit-settings-expertise', 'business-finance');

    // The quote is returned as a label containing the number of words, the
    // number of credits to charge, the total price and the currency, each of
    // these are in a separate strong markup.
    $this->assertSession()->elementContains('css', '#edit-settings-price-quote strong:nth-of-type(1)', $mocked_quote->wordcount);
    $this->assertSession()->elementContains('css', '#edit-settings-price-quote strong:nth-of-type(2)', $mocked_quote->credits);
    $this->assertSession()->elementContains('css', '#edit-settings-price-quote strong:nth-of-type(3)', $mocked_quote->price . '€');
  }

  /**
   * Test that translations can be correctly pulled.
   */
  public function testTranslationsPulling() {
    $this->loginAsAdmin();
    $this->translator->setSetting('api_public_key', 'correct key');
    $this->translator->setSetting('api_secret_key', 'correct key');
    $this->translator->save();
    $job = $this->createJob();
    $job->translator = $this->translator->id();
    $job->save();
    \Drupal::state()->set('tmgmt.test_source_data', array(
      'body' => array(
        '#text' => 'Hello world',
        '#label' => 'Body',
      ),
    ));
    $item = $job->addItem('test_source', 'test', '1');

    $job->requestTranslation();

    // Pull translations from Oht.
    $this->drupalGet('admin/tmgmt/jobs/' . $job->id());
    $this->submitForm(array(), t('Pull translations'));
    $this->assertSession()->pageTextContains(t('The translation of test_source:test:1 to German is finished and can now be reviewed.'));
    $this->assertSession()->pageTextContains(t('Fetched translations for 1 job items.'));

    // Check the updated mappings of the job item.
    $remotes = RemoteMapping::loadByLocalData($job->id(), $item->id());
    $this->assertEquals(1, count($remotes));
    $remote = reset($remotes);
    $this->assertEquals(\Drupal::state()->get('tmgmt_oht_test_project_id'), $remote->getRemoteIdentifier1());
    $this->assertEquals(\Drupal::state()->get('tmgmt_oht_test_source_resource_uuid'), $remote->getRemoteIdentifier2());
  }

  /**
   * Tests the UI of the plugin.
   */
  public function testOhtUi() {
    $this->loginAsAdmin();
    $this->drupalGet('admin/tmgmt/translators/manage/oht');

    // Try to connect with invalid credentials.
    $edit = [
      'settings[api_public_key]' => 'wrong key',
      'settings[api_secret_key]' => 'wrong key',
    ];
    $this->submitForm($edit, t('Connect'));
    $this->assertSession()->pageTextContains(t('The "OHT API Public key" or "OHT API Secret key" is not valid.'));

    // Test connection with valid credentials.
    $edit = [
      'settings[api_public_key]' => 'correct key',
      'settings[api_secret_key]' => 'correct key',
    ];
    $this->submitForm($edit, t('Connect'));
    $this->assertSession()->pageTextContains('Successfully connected!');

    // Assert that default remote languages mappings were updated.
    $this->assertSession()->optionExists('edit-remote-languages-mappings-en', 'en-us')->isSelected();
    $this->assertSession()->optionExists('edit-remote-languages-mappings-de', 'de-de')->isSelected();

    $this->submitForm([], t('Save'));
    $this->assertSession()->pageTextContains(t('@label configuration has been updated.', ['@label' => $this->translator->label()]));
  }

}
